//
//  Video.swift
//  YouTubeApp
//
//  Created by Edy Cu Tjong on 6/8/19.
//  Copyright © 2019 Edy Cu Tjong. All rights reserved.
//

import UIKit

class Video: NSObject {

    var videoId: String = ""
    var videoTitle: String = ""
    var videoDescription: String = ""
    var videoThumbnailUrl: String = ""
}
