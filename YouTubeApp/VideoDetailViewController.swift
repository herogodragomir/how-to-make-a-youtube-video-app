//
//  VideoDetailViewController.swift
//  YouTubeApp
//
//  Created by Edy Cu Tjong on 6/8/19.
//  Copyright © 2019 Edy Cu Tjong. All rights reserved.
//

import UIKit
import WebKit

class VideoDetailViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var selectedVideo:Video?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let vid = self.selectedVideo {
            self.titleLabel.text = vid.videoTitle
            self.descriptionLabel.text = vid.videoDescription
            
            webView.configuration.allowsInlineMediaPlayback = true
            let myURL = URL(string: "https://www.youtube.com/embed/" + vid.videoId + "?playsinline=1")
            let youtubeRequest = URLRequest(url: myURL!)
            webView.load(youtubeRequest)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
