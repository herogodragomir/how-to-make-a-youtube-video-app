//
//  VideoModel.swift
//  YouTubeApp
//
//  Created by Edy Cu Tjong on 6/8/19.
//  Copyright © 2019 Edy Cu Tjong. All rights reserved.
//

import UIKit
import Alamofire

protocol VideoModelDelegate {
    func dataReady()
}

class VideoModel: NSObject {
    
    let apiKey = "AIzaSyDwAYMV-pcdc_Vx1QIk3sQT7UIAEUgii70"
    let uploadPlaylistId = "PLMRqhzcHGw1aLoz4pM_Mg2TewmJcMg9ua"
    
    var videoArray = [Video]()
    var delegate: VideoModelDelegate?
    
    func getFeedVideos() {
        
        // Fetch the videos dynamically through the YouTube Data API
        Alamofire.request("https://www.googleapis.com/youtube/v3/playlistItems", method: .get, parameters: ["part":"snippet", "playlistId":uploadPlaylistId, "key":apiKey, "maxResults":50], encoding: URLEncoding.queryString, headers: nil).responseJSON { (response) in
            
            if let json = response.result.value as? NSDictionary {
                
                var arrayOfVideos = [Video]()
                for item in json["items"] as! NSArray {
                    if let video = item as? NSDictionary {
                        print(item)
                        
                        // Create video object off of the JSON response
                        let videoObj = Video()
                        videoObj.videoId = video.value(forKeyPath: "snippet.resourceId.videoId") as! String
                        videoObj.videoTitle = video.value(forKeyPath: "snippet.title") as! String
                        videoObj.videoDescription = video.value(forKeyPath: "snippet.description") as! String
                        videoObj.videoThumbnailUrl = video.value(forKeyPath: "snippet.thumbnails.maxres.url") as! String
                        
                        arrayOfVideos.append(videoObj)
                    }
                }
                // When all the video objects have been constructed, assign the array to the VideoModel property
                self.videoArray = arrayOfVideos
                
                // Notify the delegate that the data is ready
                if self.delegate != nil {
                    self.delegate!.dataReady()
                }
            }
        }
    }

    func getVideos() -> [Video] {
        var videos = [Video]()
        
        // Create an empty array of Video objects
        let video1 = Video()
        
        // Assign properties
        video1.videoId = "tKQJN_jc5AY"
        video1.videoTitle = "How To Connect a Swift Tableview (UITableView) in One Minute"
        video1.videoDescription = "In this UITableView tutorial, you'll learn how to connect a Swift table view from your storyboard to your view controller.\n\nThis is the first of a few table view tutorials in this one-minute series. Stay tuned for how to display data in the table view!"
        
        // Append it into the videos array
        videos.append(video1)
        
        
        // Create an empty array of Video objects
        let video2 = Video()
        
        // Assign properties
        video2.videoId = "hw9RrXvxuUc"
        video2.videoTitle = "How To Set The Delegate & Data Source of a Tableview (UITableView) in One Minute"
        video2.videoDescription = "In this UITableView tutorial, you'll learn how to set the Delegate and Data Source of a Swift table view\n\nThis is the second of a few table view tutorials in this one-minute series. Stay tuned for how to display data in the table view!"
        
        // Append it into the videos array
        videos.append(video2)
        
        
        // Create an empty array of Video objects
        let video3 = Video()
        
        // Assign properties
        video3.videoId = "cJUmrdfgbbA"
        video3.videoTitle = "How To Display Data in a Tableview (UITableView) in One Minute"
        video3.videoDescription = "In this UITableView tutorial, you'll learn how to display a list of data in a Swift table view\n\nThis is the third of a few table view tutorials in this one-minute series. Stay tuned for more to come!"
        
        // Append it into the videos array
        videos.append(video3)
        
        
        // Create an empty array of Video objects
        let video4 = Video()
        
        // Assign properties
        video4.videoId = "Mtxqwdjunoo"
        video4.videoTitle = "Add Rounded Corners to your Buttons (UIButtons) in One Minute"
        video4.videoDescription = "In this UIButton tutorial, you'll learn how to display a button with rounded corners in code.\n\nThis is the 4th tutorials in this one-minute series. Stay tuned for more to come!"
        
        // Append it into the videos array
        videos.append(video4)
        
        return videos
    }
}
